#ifndef PLATFORM_GL_H
#define PLATFORM_GL_H

#ifdef EMSCRIPTEN
#   include <GLES3/gl3.h>
#   include "emscripten.h"
#else
// Make sure GLAD is included before SDL2
#include <glad/glad.h>
#endif

#endif // PLATFORM_GL_H
