# emscripten-example

SDL2 project using EnTT and Bullet Physics with the latest OpenGL.

This project makes use of the following libraries/projects:

* [SDL2](https://www.libsdl.org/)
* [GLM](https://github.com/g-truc/glm/)
* [CPM](https://github.com/cpm-cmake/CPM.cmake)
* [GLAD](https://glad.dav1d.de/)
* [EnTT](https://github.com/skypjack/entt/)
* [BULLET3](https://github.com/bulletphysics/bullet3/)

![Screenshot](screenshot.jpeg)

## Building for emscripten

First setup the Emscripten SDK into the root of the project directory:

```
git clone https://github.com/emscripten-core/emsdk
cd emsdk
./emsdk install 3.1.60
./emsdk activate 3.1.60
cd ..
```

...back in the project root directory:

```
mkdir build
cd build
cmake --preset emscripten-release ..
cmake --build .
```

...this will build 3 files (emscripten-example.wasm/.html/.js).

Finally run the build result in a different command prompt via

```
<path to emscripten sdk>/emsdk/upstream/emscripten/emrun emscripten-example.html
```
